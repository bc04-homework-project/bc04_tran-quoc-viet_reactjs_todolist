export const add_task = "add_task";
export const edit_task = "edit_task";
export const edit_task2 = "edit_task2";
export const remove_task = "remove_task";
export const finish_task = "finish_task";
export const change_theme = "change_theme";
export const update_task = "update_task";
