import {
  add_task,
  change_theme,
  edit_task,
  edit_task2,
  finish_task,
  remove_task,
  update_task,
} from "../types/todoListTypes";

export const addTask = (newTask) => ({
  type: add_task,
  newTask,
});

export const removeTask = (taskID) => ({
  type: remove_task,
  taskID,
});

export const finishTask = (taskID) => ({
  type: finish_task,
  taskID,
});

export const changeTheme = (themeID) => ({
  type: change_theme,
  themeID,
});

export const editTask = (task) => ({
  type: edit_task,
  task,
});

export const editTask2 = () => ({
  type: edit_task2,
});

export const updateTask = (task) => ({
  type: update_task,
  task,
});
