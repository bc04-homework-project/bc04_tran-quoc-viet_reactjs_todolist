import { themeArr } from "../../Themes/ThemeManager";
import { ToDoListDarkTheme } from "../../Themes/ToDoListDarkTheme";
import {
  add_task,
  change_theme,
  edit_task,
  edit_task2,
  finish_task,
  remove_task,
  update_task,
} from "../types/todoListTypes";

const initialState = {
  themeTodoList: ToDoListDarkTheme,
  taskList: [
    { id: "task-1", taskName: "task 1", done: true },
    { id: "task-2", taskName: "task 2", done: false },
    { id: "task-3", taskName: "task 3", done: true },
    { id: "task-4", taskName: "task 4", done: false },
  ],
  taskEdit: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
      // console.log('okkk222 add task', action.newTask);
      if (action.newTask.taskName.trim() == "") {
        alert("You have not written any task");
        return { ...state };
      }
      let index = state.taskList.findIndex((task) => {
        return task.taskName == action.newTask.taskName;
      });
      if (index != -1) {
        alert("This task has already existed");
        return { ...state };
      }
      let cloneTaskList = [...state.taskList, action.newTask];
      state.taskList = cloneTaskList;
      return { ...state };
    }

    case remove_task: {
      // console.log('removetask', action.taskID)
      // let index = state.taskList.findIndex((task) => {
      //   return task.id == action.taskID;
      // });
      // let cloneTaskList = [...state.taskList];
      // cloneTaskList.splice(index, 1);
      // cloneTaskList = cloneTaskList.filter(task => task.id !== action.taskID)
      // return { ...state, taskList: cloneTaskList };
      return {
        ...state,
        taskList: state.taskList.filter((task) => task.id !== action.taskID),
      };
    }

    case finish_task: {
      //   console.log('finis task', action.taskID)
      let index = state.taskList.findIndex((task) => {
        return task.id == action.taskID;
      });
      let cloneTaskList = [...state.taskList];
      cloneTaskList[index].done = true;
      return { ...state, taskList: cloneTaskList };
    }

    case edit_task: {
      return { ...state, taskEdit: action.task };
    }
    case edit_task2: {
      return { ...state, taskEdit: null };
    }

    case update_task: {
      if (action.task.taskName.trim() == "") {
        alert("You have not written any task");
        return { ...state };
      }

      let index = state.taskList.findIndex((task) => {
        return task.id == action.task.id;
      });
      if (index == -1) {
        alert("This task is not in your task list");
        return { ...state };
      }
      let cloneTaskList = [...state.taskList];
      cloneTaskList[index] = action.task;
      return { ...state, taskList: cloneTaskList };
    }

    case change_theme: {
      // console.log('change theme', action.themeID)
      let cloneTheme = themeArr.find((theme) => theme.id == action.themeID);
      //   console.log("cloneTheme: ", cloneTheme);
      if (cloneTheme) {
        state.themeTodoList = cloneTheme.theme;
      }

      return { ...state };
    }

    default:
      return state;
  }
};
