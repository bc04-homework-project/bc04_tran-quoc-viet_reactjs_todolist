import { combineReducers } from "redux";
import TodoListReducer from "./TodoListReducer";

export const rootReducer_TodoList = combineReducers({
    TodoListReducer: TodoListReducer,
})
