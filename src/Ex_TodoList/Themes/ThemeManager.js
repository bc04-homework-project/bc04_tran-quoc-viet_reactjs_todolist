import { ToDoListDarkTheme } from "./ToDoListDarkTheme";
import { ToDoListLightTheme } from "./ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "./ToDoListPrimaryTheme";

export const themeArr = [
  { id: "dark", name: "Dark Theme", theme: ToDoListDarkTheme },
  { id: "light", name: "Light Theme", theme: ToDoListLightTheme },
  { id: "primary", name: "Primary Theme", theme: ToDoListPrimaryTheme },
];
