import styled from 'styled-components';

export const Hr = styled.hr`
border-top: 0.5px solid ${props => props.theme.color};
`