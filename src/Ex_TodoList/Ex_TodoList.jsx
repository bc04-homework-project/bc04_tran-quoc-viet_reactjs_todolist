import React, { Component } from "react";
import { ThemeProvider } from "styled-components";
import { Button } from "./Components/Button";
import { Container } from "./Components/Container";
import { Dropdown } from "./Components/Dropdown";
import { Heading3 } from "./Components/Heading";
import { Hr } from "./Components/Hr";
import { Table, Th, Thead, Tr } from "./Components/Table";
import { TextField } from "./Components/TextField";
import { connect } from "react-redux/es/exports";
import {
  addTask,
  changeTheme,
  editTask,
  editTask2,
  finishTask,
  removeTask,
  updateTask,
} from "./Redux/actions/todoListActions";
import { themeArr } from "./Themes/ThemeManager";

class Ex_TodoList extends Component {
  state = {
    task: {
      taskName: "",
    },
  };

  static getDerivedStateFromProps(newProps, prevState) {
    // console.log("newProps :", newProps, "prevState: ", prevState);

    if (newProps.taskEdit) {
      return { task: newProps.taskEdit };
    }
    return null;
  }

  renderTaskTodo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(editTask(task));
                }}
              >
                <i className="fa fa-edit" />
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(finishTask(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-check" />
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(removeTask(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash" />
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskComplete = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(removeTask(task.id));
                }}
                className="ml-1"
              >
                <i className="fa fa-trash" />
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderThemeList = () => {
    return themeArr.map((theme) => {
      return (
        <option key={theme.name} value={theme.id}>
          {theme.name}
        </option>
      );
    });
  };

  render() {
    return (
      <ThemeProvider theme={this.props.themeTodoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              this.props.dispatch(changeTheme(e.target.value));
            }}
          >
            {this.renderThemeList()}
          </Dropdown>

          <Heading3>To Do List</Heading3>
          <TextField
            onChange={(e) => {
              this.setState({
                task: { ...this.state.task, taskName: e.target.value },
              });

              this.props.dispatch(editTask2());
            }}
            value={this.state.task.taskName}
            label="Task Name"
            className="w-50"
          />
          <Button
            onClick={() => {
              let newTask = { ...this.state.task, id: Date.now(), done: false };
              this.props.dispatch(addTask(newTask));
              this.setState({task: {id:'',taskName:''}})
            }}
            className="ml-2"
          >
            <i className="fa fa-plus"></i> Add Task
          </Button>
          <Button
            onClick={() => {
              this.props.dispatch(updateTask(this.state.task));
              this.setState({task: {id:'',taskName:''}})
            }}
            className="ml-2"
          >
            <i className="fa fa-upload"></i> Update Task
          </Button>
          <Hr />

          <Heading3>Task To Do</Heading3>
          <Table>
            <Thead>{this.renderTaskTodo()}</Thead>
          </Table>

          <Heading3>Task Complete</Heading3>
          <Table>
            <Thead>{this.renderTaskComplete()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }
}

const mapStateToProps = (state) => ({
  themeTodoList: state.TodoListReducer.themeTodoList,
  taskList: state.TodoListReducer.taskList,
  taskEdit: state.TodoListReducer.taskEdit,
});

export default connect(mapStateToProps)(Ex_TodoList);
